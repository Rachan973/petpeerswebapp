package com.casestudy.exception;

public class PetException extends Exception {

	/**
	 * Handling Pet Exceptions
	 */
	private static final long serialVersionUID = 1L;
	private String message;

	public PetException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
