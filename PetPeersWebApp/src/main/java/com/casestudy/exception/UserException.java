package com.casestudy.exception;

public class UserException extends Exception{
	
	/**
	 * Handling user Exceptions
	 */
	
	private String message;

	public UserException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
