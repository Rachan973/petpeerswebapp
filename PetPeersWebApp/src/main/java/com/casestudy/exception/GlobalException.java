package com.casestudy.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalException {

	@ExceptionHandler({PetException.class, UserException.class})
	public ModelAndView processException(Exception exception)
	{
		ModelAndView modelAndView = new ModelAndView("globalException");
		modelAndView.addObject("exceptionCause",exception.getMessage());
		return modelAndView;
	}
}
