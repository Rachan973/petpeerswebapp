package com.casestudy.dao;

import java.util.List;

import com.casestudy.model.Pet;

/**
 * create dao interface for pet
 * @author Rachan
 *
 */

public interface PetDao {

	public abstract Pet savePet(Pet pet);

	public abstract Pet buyPet(int userId, int petId);

	public abstract List<Pet> getAllPets();

	public abstract List<Pet> getMyPets(int userId);

}
