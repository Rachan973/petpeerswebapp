package com.casestudy.dao;

import com.casestudy.model.User;

/**
 * Data Access Object(DAO) class
 * 
 * @author Pragati Sharma
 *
 */
public interface UserDao {
	public abstract User saveUser(User user);

	public abstract User authenticateUser(String userName, String password);
}