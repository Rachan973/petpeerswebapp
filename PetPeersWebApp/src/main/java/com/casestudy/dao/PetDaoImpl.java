package com.casestudy.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.casestudy.model.Pet;
import com.casestudy.model.User;

@Repository
public class PetDaoImpl implements PetDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private User user;

	@Override
	public Pet savePet(Pet pet) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(pet);
		return pet;
	}

	@Override
	public Pet buyPet(int userId, int petId) {
		Session session = this.sessionFactory.getCurrentSession();	
		Pet pet = (Pet) session.load(Pet.class, (long) petId);
        if (pet != null) {
        	user.setUserId((long) userId);
        	pet.setUser(user);
            this.sessionFactory.getCurrentSession().update(pet);
        }
		
         return pet;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Pet> getAllPets() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Pet> petList = session.createQuery("from Pet").list();
		return petList;
	}

	@Override
	public List<Pet> getMyPets(int userId) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Pet> petList = null;
		// Query using Hibernate Query Language
		String SQL_QUERY = " from Pet  where PETOWNERID = ?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0, userId);
		@SuppressWarnings("unchecked")
		List<Pet> myPets = query.getResultList();

		if (myPets != null) {
			petList = myPets;
		}

		return petList;
	}

}
