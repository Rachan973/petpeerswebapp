package com.casestudy.dao;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.casestudy.model.User;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public User authenticateUser(String userName, String password) {
		Session session = sessionFactory.openSession();
		User userFound = null;
		// Query using Hibernate Query Language
		String SQL_QUERY = " from User  where userName=? and userPassword=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0, userName);
		query.setParameter(1, password);
		User user = (User) query.getSingleResult();

		if (user != null) {
			userFound = user;
		}

		return userFound;
	}

	@Override
	public User saveUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(user);
		return user;
	}

	
}
