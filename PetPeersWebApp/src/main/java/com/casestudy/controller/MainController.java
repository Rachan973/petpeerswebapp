package com.casestudy.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.executable.ValidateOnExecution;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.casestudy.exception.PetException;
import com.casestudy.exception.UserException;
import com.casestudy.model.Pet;
import com.casestudy.model.User;
import com.casestudy.service.PetService;
import com.casestudy.service.UserService;

/**
 * Controller class using spring framework annotations
 * 
 * @author Rachan
 *
 */

@Controller
public class MainController {

	private static final Logger logger = LogManager.getLogger(MainController.class);

	@Autowired
	private Pet pet;

	@Autowired
	private PetService petService;

	@Autowired
	private User user;

	@Autowired
	private UserService userService;

	@GetMapping(value = "/")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("index");
		logger.info("Show index page");
		modelAndView.addObject("user", user);
		return modelAndView;

	}

	// UserLogin Process

	@GetMapping(value = "/loginPage")
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("loginPage");
		logger.info("Show login page");
		modelAndView.addObject("user", user);
		return modelAndView;

	}

	@RequestMapping(path = "/authenticateUser", method = RequestMethod.POST)
	public ModelAndView authenticateUser(HttpServletRequest request, User user, BindingResult bindingResult)
			throws UserException {

		ModelAndView modelAndView = new ModelAndView();
		if (bindingResult.hasErrors()) {
			logger.error("error in data");
			modelAndView.setViewName("loginPage");

		} else {
			String userName = request.getParameter("userName");
			String userPassword = request.getParameter("userPassword");
			User currentUser = this.userService.authenticateUser(userName, userPassword);
			logger.info("Login done");
			HttpSession httpSession = request.getSession();
			httpSession.setAttribute("currentUser", currentUser);
			logger.info("login session stores" + currentUser);
			modelAndView.addObject("uname", currentUser.getUserName());
			modelAndView.setViewName("index");
		}
		return modelAndView;
	}

	// UserRegistration Process

	@GetMapping("/registrationPage")
	public ModelAndView register() {
		ModelAndView modelAndView = new ModelAndView("registrationPage");
		logger.info("Show registration page");
		modelAndView.addObject("user", user);
		return modelAndView;
	}

	@PostMapping(path = "/saveUser")
	public ModelAndView saveUser(User user, BindingResult bindingResult) throws UserException {

		ModelAndView modelAndView = new ModelAndView();
		if (bindingResult.hasErrors()) {
			logger.error("error in data");
			modelAndView.setViewName("registrationPage");
		} else {
			this.userService.saveUser(user);
			logger.info("User saved");
			modelAndView.setViewName("loginPage");
		}
		return modelAndView;
	}

	@GetMapping(value = "/home")
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("homePage");
		List<Pet> petsList = petService.getAllPets();
		modelAndView.addObject("petList", petsList);
		logger.info("Show home page");
		return modelAndView;

	}

	@GetMapping(value = "/addPet")
	@ValidateOnExecution
	public ModelAndView addPet() {
		ModelAndView modelAndView = new ModelAndView("addPetPage");
		modelAndView.addObject("pet", pet);
		logger.info("Show add pet page");
		return modelAndView;

	}

	@PostMapping(value = "/savePet")
	public ModelAndView savePet(@ModelAttribute("pet") Pet pet) throws PetException {
		ModelAndView modelAndView = new ModelAndView();
		petService.savePet(pet);
		logger.info("Pet saved");
		modelAndView.setViewName("redirect:/home");
		return modelAndView;

	}

	/**
	 * Buy pet method
	 * 
	 * @param httpServletRequest
	 * @return
	 * @throws PetException
	 */

	@GetMapping(value = "/buyPet")
	public ModelAndView buyPet(HttpServletRequest httpServletRequest) throws PetException {
		ModelAndView modelAndView = new ModelAndView("redirect:/home");
		logger.info("buy Pet");
		HttpSession httpSession = httpServletRequest.getSession();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		if (currentUser != null) {
			String stringPetId = httpServletRequest.getParameter("petId");
			int petId = Integer.parseInt(stringPetId);
			logger.info("Pet Id" + petId);
			long userId = currentUser.getUserId();
			logger.info("User Id" + userId);
			this.petService.buyPet((int) userId, petId);
			logger.error("error in buy get");
		}
		return modelAndView;

	}

	@GetMapping(value = "/myPets")
	public ModelAndView petList(HttpServletRequest httpServletRequest) throws PetException {

		ModelAndView modelAndView = new ModelAndView("myPetsPage");
		logger.info("my pet list");
		HttpSession httpSession = httpServletRequest.getSession();
		User currentUser = (User) httpSession.getAttribute("currentUser");
		long userId = currentUser.getUserId();
		logger.info("User Id" + userId);
		List<Pet> myPetsList = this.petService.getMyPets((int) userId);
		for (Pet pet : myPetsList) {
			System.out.println("pet" + pet.getPetId());
		}
		modelAndView.addObject("myPetsList", myPetsList);
		return modelAndView;

	}

	@GetMapping(value = "/logout")
	public ModelAndView logout(HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView();
		HttpSession httpSession = request.getSession();
		httpSession.invalidate();
		logger.info("Logout");
		modelAndView.setViewName("redirect:/loginPage");
		return modelAndView;
	}

	@GetMapping(value = "*")
	public ModelAndView bussinessForAll(Model model) {

		ModelAndView modelAndView = new ModelAndView("fallback");
		return modelAndView;
	}

}
