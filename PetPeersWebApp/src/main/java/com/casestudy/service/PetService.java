package com.casestudy.service;

import java.util.List;

import com.casestudy.exception.PetException;
import com.casestudy.model.Pet;

/**
 * create pet interface for pet
 * @author Rachan
 *
 */

public interface PetService {
	
	public abstract Pet savePet(Pet pet) throws PetException;
	
	public abstract Pet buyPet(int userId, int petId) throws PetException;
	
	public abstract List<Pet> getAllPets();
	
	public abstract List<Pet> getMyPets(int userId) throws PetException;
	

}
