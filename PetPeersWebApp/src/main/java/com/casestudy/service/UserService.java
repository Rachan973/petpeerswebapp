package com.casestudy.service;

import com.casestudy.exception.UserException;
import com.casestudy.model.User;

public interface UserService {


	 public abstract User saveUser(User user) throws UserException;
	 public abstract User authenticateUser(String userName, String password) throws UserException;

	

}
