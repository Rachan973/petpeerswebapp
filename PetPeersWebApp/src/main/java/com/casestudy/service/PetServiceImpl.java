package com.casestudy.service;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.casestudy.dao.PetDao;
import com.casestudy.exception.PetException;
import com.casestudy.model.Pet;

/**
 * implementation of petService class
 * 
 * @author Rachan
 *
 */
@Service
public class PetServiceImpl implements PetService {

	private static final Logger logger = LogManager.getLogger(PetServiceImpl.class);

	@Autowired
	private PetDao petDao;

	@Override
	@Transactional
	public Pet savePet(Pet pet) throws PetException {
		Pet petDaoObject = null;
		if (pet != null) {
			if (pet.getPetName().equals(null) && pet.getPetPlace().equals(null) && ((Integer)pet.getPetAge()).equals(null)) {
				
				logger.error("pet name or place are null");
				throw new PetException("Pet Name,Age and Place are not null");
				
			}
			else  {
				if (pet.getPetAge() >= 0 && pet.getPetAge() <= 99) {
					logger.info("Pet name place are validated" + "Pet age" +pet.getPetAge());
					petDaoObject = this.petDao.savePet(pet);
				}
				else  {
					logger.error("Add valid age");
					throw new PetException("Add valid age");
				} 
				
			} 
		}
		return petDaoObject;
	}

	@Override
	@Transactional
	public Pet buyPet(int userId, int petId) throws PetException {
		Pet petDaoObject = null;
		if (userId > 0 && petId > 0) {
			petDaoObject = this.petDao.buyPet(userId, petId);
		} else {
			throw new PetException("invalid user or pet");

		}
		return petDaoObject;
	}

	@Override
	@Transactional
	public List<Pet> getAllPets() {

		List<Pet> petList = petDao.getAllPets();
		return petList;
	}

	@Override
	@Transactional
	public List<Pet> getMyPets(int userId) throws PetException {
		List<Pet> petList = null;
		if (userId >= 0) {
			petList = petDao.getMyPets(userId);
		} else {
			throw new PetException("Please Login");
		}
		return petList;
	}

}
