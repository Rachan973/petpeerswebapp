package com.casestudy.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casestudy.dao.UserDao;
import com.casestudy.exception.UserException;
import com.casestudy.model.User;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger logger = LogManager.getLogger(PetServiceImpl.class);

	@Autowired
	private UserDao userDao;

	@Override
	@Transactional
	public User saveUser(User user) throws UserException {
		if (user != null) {
			logger.info("user data validated");
			return this.userDao.saveUser(user);
		} else {
			logger.error("Insufficient data");
			throw new UserException("Insufficient data");
		}
	}

	@Override
	@Transactional
	public User authenticateUser(String userName, String password) throws UserException {
		if (userName != " " && password != " ") {
			logger.info("user data validated");
			return this.userDao.authenticateUser(userName, password);
		} else {
			logger.error("Insufficient data");
			throw new UserException("Insufficient data");
		}
	}
}
