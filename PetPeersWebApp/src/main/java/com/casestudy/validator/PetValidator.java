package com.casestudy.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.casestudy.model.Pet;
/**
 * Custom validations for pet class
 * @author Rachan
 *
 */
public class PetValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		return false;
	}

	@Override
	public void validate(Object obj, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "petName", "petName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "petAge", "petAge.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "petPlace", "petPlace.required");
    	Pet pet = (Pet) obj;
		 if (pet.getPetAge() <= 0 && pet.getPetAge() >= 99) {
        	
        	System.out.println("invaliad age");
			errors.rejectValue("petAge", "age.invalid");
		}
		
	}

}
