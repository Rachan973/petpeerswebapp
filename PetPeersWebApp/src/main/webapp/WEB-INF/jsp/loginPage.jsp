<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>

<title>Login Page</title>
<jsp:include page="header.jsp" />
<div class="container">
	<h2>Login</h2>
	<springForm:form method="POST" modelAttribute="user"
		action="authenticateUser">
		<table>
			<tr>
				<td>User Name:</td>
				<td><springForm:input path="userName" class="form-control"/></td>
				<td><springForm:errors path="userName" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><springForm:input path="userPassword" type="password" class="form-control"/></td>
				<td><springForm:errors path="userPassword" cssClass="error" /></td>
			</tr>


			<tr>
				<td colspan="3"><input type="submit" value="Login" class="btn"></td>
			</tr>

		</table>
	</springForm:form>

</div>
<jsp:include page="footer.jsp" />