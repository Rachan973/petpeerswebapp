<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>

<title>Registration Page</title>
<jsp:include page="header.jsp" />
<div class="container">
	<h2>Register Here</h2>
	<springForm:form method="POST" modelAttribute="user" action="saveUser">
		<table>
			<tr class="mb-10">
				<td>User Name:</td>
				<td><springForm:input path="userName" class="form-control" /></td>
				<td><springForm:errors path="userName" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><springForm:input path="userPassword" type="password"
						class="form-control" /></td>
				<td><springForm:errors path="userPassword" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Confirm Password:</td>
				<td><springForm:input path="confirmPassword" type="password"
						class="form-control" /></td>
				<td><springForm:errors path="confirmPassword" cssClass="error" /></td>
			</tr>

			<tr>
				<td colspan="4"><input type="submit" class="btn"
					value="Register"></td>
			</tr>

		</table>
	</springForm:form>
</div>
<jsp:include page="footer.jsp" />