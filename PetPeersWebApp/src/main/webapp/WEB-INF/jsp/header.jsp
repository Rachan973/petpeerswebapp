<%@page import="com.google.protobuf.Empty"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Pet Peers Web App</title>
<!-- Bootstrap CSS -->
<link href='<c:url value="/resources/css/style.css" />' rel="stylesheet">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	<%
		String uname = (String) request.getAttribute("uname");

		if (uname != null) {

			session.setAttribute("user", uname);
		}
	%>
	<nav class="navbar navbar-inverse" style="margin-bottom: 0px;">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="./">Pet Peers</a>
		</div>

		<ul class="nav navbar-nav navbar-right">
			<%
				String name = (String) session.getAttribute("user");
				if (name != null) {
			%>
			<li class="active"><a href="home">Home</a></li>

			<li><a href="addPet">Add Pet</a></li>

			<li><a href="myPets">My Pets</a></li>
			<li><a href="logout"><span
					class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			<%
				} else {
			%>
			<li><a href="registrationPage"><span
					class="glyphicon glyphicon-user"></span> Sign Up</a></li>
			<li><a href="loginPage"><span
					class="glyphicon glyphicon-log-in"></span> Login</a></li>
			<%
				}
			%>
		</ul>
	</div>
	</nav>
	<div class="container">
		<br>

		<%
			if (name != null) {
				out.print("Welcome " + name);
			}
		%>
	</div>