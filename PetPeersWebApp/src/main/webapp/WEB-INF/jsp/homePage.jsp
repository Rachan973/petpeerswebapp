<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="header.jsp" />
<div class="container">

	<h3>Pet List ${ uname}</h3>
	<c:choose>
		<c:when test="${!empty petList}">
			<table id="dtBasicExample"
				class="table table-striped table-bordered table-sm" cellspacing="0"
				width="100%">
				<thead>
					<tr>
						<th width="80">Pet ID</th>
						<th width="120">Pet Name</th>
						<th width="60">Pet Age</th>
						<th width="120">Pet Place</th>
						<th width="60">Buy</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${petList}" var="pet">
						<tr>
							<td>${pet.petId}</td>
							<td>${pet.petName}</td>
							<td>${pet.petAge}</td>
							<td>${pet.petPlace}</td>
							<td><c:choose>
									<c:when test="${empty pet.user.userId}">
										<a href="<c:url value='./buyPet?petId=${pet.petId}' />
				 						">Buy</a>
									</c:when>
									<c:otherwise>
										<p>Sold out</p>
									</c:otherwise>
								</c:choose></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

		</c:when>
		<c:otherwise>
			<p>No data is available</p>
		</c:otherwise>
	</c:choose>
</div>
<jsp:include page="footer.jsp" />