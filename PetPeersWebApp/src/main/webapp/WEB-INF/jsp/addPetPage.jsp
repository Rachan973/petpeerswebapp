<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>
<jsp:include page="header.jsp" />

<title>Add Pet</title>
<div class="container">
	<springForm:form method="POST" modelAttribute="pet"
		action="savePet">
		<table>
			<tr>
				<td>Name:</td>
				<td><springForm:input path="petName" class="form-control"/></td>
				<td><springForm:errors path="petName" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Age:</td>
				<td><springForm:input path="petAge" class="form-control"/></td>
				<td><springForm:errors path="petAge" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Place:</td>
				<td><springForm:input path="petPlace" class="form-control"/></td>
				<td><springForm:errors path="petPlace" cssClass="error" /></td>
			</tr>
			
			<tr>
				<td colspan="3"><input type="submit" value="Add Pet" class="btn"></td>
			</tr>
			<tr>
				<td colspan="3"><input type="reset" value="Cancel" class="btn"></td>
			</tr>
		</table>
	</springForm:form>
</div>
<jsp:include page="footer.jsp" />