<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<jsp:include page="header.jsp" />
<title>My Pets List</title>
<div class="container">
	<h3>My Pet List</h3>

	<c:choose>
	<c:when test="${!empty myPetsList}">
		<table id="dtBasicExample"
			class="table table-striped table-bordered table-sm" cellspacing="0"
			width="100%">
			<thead>
				<tr>
					<th width="80">Pet Id</th>
					<th width="120">Pet Name</th>
					<th width="60">Pet Age</th>
					<th width="120">Pet Place</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${myPetsList}" var="pet">
					<tr>
						<td>${pet.petId}</td>
						<td>${pet.petName}</td>
						<td>${pet.petAge}</td>
						<td>${pet.petPlace}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
</c:when>
	<c:otherwise>
		<p>No data is available</p>
	</c:otherwise>
</c:choose>

</div>
<jsp:include page="footer.jsp" />